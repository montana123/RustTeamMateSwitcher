using Newtonsoft.Json;
using Oxide.Core;
using Oxide.Core.Libraries.Covalence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("Team Mate Switcher", "Phate", "1.0.0")]
    [Description("Plugin to play trio Server with more than 3 people")]
    public class TeamMateSwitcher : RustPlugin
    {
        #region Variables

        private bool debug = false;
        private Configuration config;
        private List<Team> teams = new List<Team>();

        private const string permIgnoreTeamLimit = "teammateswitcher.ignore";

        #endregion

        #region Class

        private class Configuration
        {
            [JsonProperty("Logout Timer", ObjectCreationHandling = ObjectCreationHandling.Replace)]
            public int LogoutTimer { get; set; } = 20;

            [JsonProperty("Max Players per Team (minimum 1 Player)", ObjectCreationHandling = ObjectCreationHandling.Replace)]
            public int MaxPlayersPerTeam { get; set; } = 3;
        }

        #endregion

        #region Models
        private class Team
        {
            public ulong TeamID { get; set; }
            public List<PlayerData> Players = new List<PlayerData>();
        }

        private class PlayerData
        {
            public ulong UserId { get; set; }
            public bool IsOnline { get; set; }
            public DateTime LogoutTime { get; set; }
        }

        #endregion

        #region Init
        private void Init()
        {
            permission.RegisterPermission(permIgnoreTeamLimit, this);

            Puts("Loaded TeamMateSwitcher with Config:");
            Puts("MaxPlayerPerTeam: " + config.MaxPlayersPerTeam);
            Puts("LogoutTimer: " + config.LogoutTimer);

            if (config.MaxPlayersPerTeam < 1)
                Puts("MaxPlayerPerTeam lower then 1!");

            getAllTeams();

            cleanAllTeams();
        }

        #endregion

        #region Events

        protected override void LoadDefaultConfig() => config = new Configuration();

        void OnTeamCreated(BasePlayer player, RelationshipManager.PlayerTeam team)
        {
            LogInfo(player.displayName + " (" + player.UserIDString + ") - Created a Team");
            if (debug)
                Puts("OnTeamCreated");
            addTeamToTeams(team);
        }

        object OnTeamAcceptInvite(RelationshipManager.PlayerTeam team, BasePlayer player)
        {
            if (debug)
                Puts("OnTeamAcceptInvite");
            // Before Invite:
            int playerCount = team.GetOnlineMemberConnections().Count;
            bool canAcceptInvite = true;

            if (debug)
                Puts("OnlinePlayerCount: " + playerCount + " - MaxPlayerPerTeam: " + config.MaxPlayersPerTeam);
            if (playerCount + 1 > config.MaxPlayersPerTeam)
            {
                SendReply(player, "Max team limit reached! Disconnect " + ((playerCount + 1) - config.MaxPlayersPerTeam) + " player/s to invite a new member!");
                BasePlayer teamLeader = team.GetLeader();
                if (teamLeader != null)
                    LogInfo($"{player.displayName} ({player.UserIDString}) - could not accept the Team invite from {teamLeader.displayName} ({teamLeader.UserIDString}) - Reason: OnlinePlayerCount: {playerCount} - MaxPlayerPerTeam: {config.MaxPlayersPerTeam}");
                return false;
            }

            Team foundTeam = getTeamFromTeamID(team);

            List<PlayerData> onlinePlayers = new List<PlayerData>();

            foreach (var foundPlayer in foundTeam.Players)
            {
                if (foundPlayer.UserId == player.userID) // Same user already in team
                    continue;

                if (!foundPlayer.IsOnline)
                {
                    if (debug)
                        Puts(foundPlayer.LogoutTime.ToString());
                    double minutesTillLogOut = calculateTimeSpanFromNow(foundPlayer.LogoutTime);

                    if (minutesTillLogOut < config.LogoutTimer)
                    {
                        onlinePlayers.Add(foundPlayer);
                        double waitTimer = Math.Round((config.LogoutTimer - minutesTillLogOut), 2);
                        SendReply(player, "A player is still on Logouttimer! Time remaining: " + getMinuteStringFromDouble(waitTimer).ToString());
                        canAcceptInvite = false;
                        break;
                    }
                }
                else
                {
                    onlinePlayers.Add(foundPlayer);
                }
            }

            foundTeam.Players = onlinePlayers;

            if (canAcceptInvite)
            {
                PlayerData playerData = new PlayerData();
                playerData.UserId = player.userID;
                foundTeam.Players.Add(playerData);
            }

            if (canAcceptInvite)
            {
                BasePlayer teamLeader = team.GetLeader();
                if (teamLeader != null)
                    LogInfo($"{player.displayName} ({player.UserIDString}) - accepted the Team invite from {teamLeader.displayName} ({teamLeader.UserIDString})");
                return null;
            }
            else
            {
                BasePlayer teamLeader = team.GetLeader();
                if (teamLeader != null)
                    LogInfo($"{player.displayName} ({player.UserIDString}) - could not accept the Team invite from {teamLeader.displayName} ({teamLeader.UserIDString}) - Reason: OnlinePlayerCount: {playerCount} - MaxPlayerPerTeam: {config.MaxPlayersPerTeam}");
                return canAcceptInvite;
            }
        }

        void OnTeamDisbanded(RelationshipManager.PlayerTeam team)
        {
            BasePlayer teamLeader = team.GetLeader();
            if (teamLeader != null)
                LogInfo($"{teamLeader.displayName} ({teamLeader.UserIDString}) disbanded his Team!");

            if (debug)
                Puts("OnTeamDisbanded");
            Team foundTeam = teams.Find(x => x.TeamID == team.teamID);

            if (foundTeam != null)
                teams.Remove(foundTeam);
        }

        object OnTeamLeave(RelationshipManager.PlayerTeam team, BasePlayer player)
        {
            BasePlayer teamLeader = team.GetLeader();
            if (teamLeader != null)
                LogInfo($"{player.displayName} ({player.UserIDString}) - left the Team from {teamLeader.displayName} ({teamLeader.UserIDString})");

            if (debug)
                Puts("OnTeamLeave");
            Team foundTeam = getTeamFromTeamID(player.Team);

            if (team.members.Count == 1 && foundTeam != null)
            {
                teams.Remove(foundTeam);
            }

            return null;
        }

        void OnUserDisconnected(IPlayer player)
        {
            BasePlayer basePlayer = BasePlayer.FindByID(ulong.Parse(player.Id));

            if (basePlayer.Team != null)
            {
                Team foundTeam = getTeamFromTeamID(basePlayer.Team);

                PlayerData playerData = foundTeam.Players.Find(x => x.UserId == basePlayer.userID);

                if (playerData == null)
                {
                    if (foundTeam.Players.Count >= config.MaxPlayersPerTeam)
                    {

                        return;
                    }

                    PlayerData newData = new PlayerData();
                    newData.UserId = basePlayer.userID;
                    newData.LogoutTime = DateTime.Now;
                    newData.IsOnline = false;

                    foundTeam.Players.Add(newData);
                }
                else
                {
                    playerData.LogoutTime = DateTime.Now;
                    playerData.IsOnline = false;
                }
            }
        }

        void OnUserConnected(IPlayer player)
        {
            BasePlayer basePlayer = BasePlayer.FindByID(ulong.Parse(player.Id));
            if (permission.UserHasPermission(basePlayer.UserIDString, permIgnoreTeamLimit))
                return;

            if (basePlayer.Team != null)
            {
                if (debug)
                    Puts("OnlinePlayerCount: " + basePlayer.Team.GetOnlineMemberConnections().Count + " - MaxPlayerPerTeam: " + config.MaxPlayersPerTeam);

                if (basePlayer.Team.GetOnlineMemberConnections().Count > config.MaxPlayersPerTeam)
                {
                    KickPlayer(basePlayer, "Max team limit reached!");
                    return;
                }

                Team foundTeam = getTeamFromTeamID(basePlayer.Team);

                foreach (var playerData in foundTeam.Players)
                {
                    if (playerData.UserId == basePlayer.userID)
                    {
                        playerData.IsOnline = true;
                        return;
                    }
                }

                List<PlayerData> newPlayerData = new List<PlayerData>();

                foreach (var playerData in foundTeam.Players)
                {
                    if (!playerData.IsOnline)
                    {
                        if (debug)
                            Puts(playerData.LogoutTime.ToString());

                        double minutesTillLogOut = calculateTimeSpanFromNow(playerData.LogoutTime);
                        if (debug)
                            Puts("MinutesTillLogout: " + minutesTillLogOut + " - LogoutTimer: " + config.LogoutTimer);

                        if (minutesTillLogOut < config.LogoutTimer)
                        {
                            newPlayerData.Add(playerData);
                            double waitTimer = Math.Round((config.LogoutTimer - minutesTillLogOut), 2);
                            KickPlayer(basePlayer, "A player is still on Logouttimer! Time remaining: " + getMinuteStringFromDouble(waitTimer).ToString());
                            return;
                        }
                    }
                    else
                    {
                        newPlayerData.Add(playerData);
                    }
                }

                foundTeam.Players = newPlayerData;
            }
        }

        object OnTeamKick(RelationshipManager.PlayerTeam team, BasePlayer player, ulong target)
        {
            if (debug)
                Puts("OnTeamKick");

            BasePlayer teamLeader = team.GetLeader();
            if (teamLeader != null)
                LogInfo($"{player.displayName} ({player.UserIDString}) - left the Team from {teamLeader.displayName} ({teamLeader.UserIDString})");

            Team foundTeam = getTeamFromTeamID(team);
            PlayerData playerData = foundTeam.Players.Find(x => x.UserId == target);

            if (playerData == null)
            {
                PlayerData newData = new PlayerData();
                newData.UserId = target;
                newData.LogoutTime = DateTime.Now;
                newData.IsOnline = false;

                foundTeam.Players.Add(newData);
            }
            else
            {
                playerData.LogoutTime = DateTime.Now;
                playerData.IsOnline = false;
            }

            return null;
        }

        #endregion

        #region HelperFunctions

        protected override void LoadConfig()
        {
            base.LoadConfig();
            try
            {
                config = Config.ReadObject<Configuration>();
                if (config == null) throw new Exception();
            }
            catch
            {
                Config.WriteObject(config, false, $"{Interface.Oxide.ConfigDirectory}/{Name}.jsonError");
                PrintError("The configuration file contains an error and has been replaced with a default config.\n" +
                           "The error configuration file was saved in the .jsonError extension");
                LoadDefaultConfig();
            }

            SaveConfig();
        }

        protected override void SaveConfig() => Config.WriteObject(config);

        private void getAllTeams()
        {
            foreach (BasePlayer player in BasePlayer.activePlayerList)
            {
                if (player.Team != null)
                {
                    if (teams.Find(x => x.TeamID == player.Team.teamID) == null)
                    {
                        Team team = new Team();
                        team.TeamID = player.Team.teamID;
                        if (debug)
                            Puts("Team:" + team.TeamID);
                        foreach (var member in player.Team.members)
                        {
                            BasePlayer memberBasePlayer = BasePlayer.FindByID(member);
                            if (memberBasePlayer == null || !memberBasePlayer.IsConnected)
                                continue;
                            PlayerData playerData = new PlayerData();
                            playerData.UserId = member;
                            team.Players.Add(playerData);
                        }
                        teams.Add(team);
                    }
                }
            }

            if (debug)
            {
                foreach (Team team in teams)
                {
                    Puts("Team: " + team.TeamID);
                    foreach (PlayerData player in team.Players)
                    {
                        BasePlayer basePlayer = BasePlayer.FindByID(player.UserId);
                        Puts("PlayerID: " + player.UserId);
                        if (basePlayer != null)
                            Puts("PlayerName: " + basePlayer.name);
                    }
                }
            }
        }

        /// <summary>
        /// Cleans all Teams from too much Players! Usefull on loading this Plugin after already running the Server.
        /// </summary>
        private void cleanAllTeams()
        {
            List<ulong> teamsChecked = new List<ulong>();
            foreach (var player in BasePlayer.activePlayerList)
            {
                if (player.Team != null)
                {
                    if (!teamsChecked.Contains(player.Team.teamID))
                    {
                        Puts("OnlinePlayerCount: " + player.Team.GetOnlineMemberConnections().Count + " - MaxPlayerPerTeam: " + config.MaxPlayersPerTeam);
                        if (player.Team.GetOnlineMemberConnections().Count > config.MaxPlayersPerTeam)
                        {
                            int memberToKick = config.MaxPlayersPerTeam - player.Team.GetOnlineMemberConnections().Count;
                            BasePlayer teamLeader = player.Team.GetLeader();
                            foreach (var member in player.Team.members)
                            {
                                BasePlayer memberBasePlayer = BasePlayer.FindByID(member);
                                if (teamLeader != memberBasePlayer)
                                {
                                    KickPlayer(memberBasePlayer, "Max team limit reached!");
                                    memberToKick--;

                                    if (memberToKick == 0)
                                        break;
                                }
                            }
                        }
                        teamsChecked.Add(player.Team.teamID);
                    }
                }
            }
        }

        private void addTeamToTeams(RelationshipManager.PlayerTeam team)
        {
            Team newTeam = new Team();
            newTeam.TeamID = team.teamID;

            if (teams.Find(x => x.TeamID == team.teamID) == null)
            {
                teams.Add(newTeam);
            }
        }

        bool wasPlayerInTeam(Team team, ulong playerId)
        {
            foreach (var data in team.Players)
            {
                if (data.UserId == playerId)
                    return true;
            }

            return false;
        }

        private void KickPlayer(BasePlayer player, string message)
        {
            player.Kick(message);
        }

        private Team getTeamFromTeamID(RelationshipManager.PlayerTeam team)
        {
            Team foundTeam = teams.Find(x => x.TeamID == team.teamID);

            if (foundTeam == null)
            {
                addTeamToTeams(team);

                foundTeam = teams.Find(x => x.TeamID == team.teamID);
            }

            return foundTeam;
        }

        private double calculateTimeSpanFromNow(DateTime to)
        {
            TimeSpan span = to - DateTime.Now;
            double totalMinutes = span.TotalMinutes;
            return totalMinutes * -1;
        }

        private string getMinuteStringFromDouble(double waitTime)
        {
            waitTime = waitTime * 60;
            int minutes = (int)waitTime / 60;
            int seconds = (int)waitTime % 60;
            return minutes + "m" + seconds + "s";
        }

        /// <summary>
        /// Log an error message to the logfile
        /// </summary>
        /// <param name="aMessage"></param>
        private void LogError(string aMessage) => LogToFile(string.Empty, $"[{DateTime.Now.ToString("dd.MM.yyyy hh:mm:ss")}] ERROR > {aMessage}", this);

        /// <summary>
        /// Log an informational message to the logfile
        /// </summary>
        /// <param name="aMessage"></param>
        private void LogInfo(string aMessage) => LogToFile(string.Empty, $"[{DateTime.Now.ToString("dd.MM.yyyy hh:mm:ss")}] INFO > {aMessage}", this);

        /// <summary>
        /// Log a debugging message to the logfile
        /// </summary>
        /// <param name="aMessage"></param>
        private void LogDebug(string aMessage)
        {
            if (debug)
                LogToFile(string.Empty, $"[{DateTime.Now.ToString("dd.MM.yyyy hh:mm:ss")}] DEBUG > {aMessage}", this);
        }
        #endregion
    }
}
